var express = require('express'),
    app     = require('./api/app'),
    config  = require('./config');

// Client static files
app.configure(function () {
  app.use(express.static(__dirname + '/client'));
  app.use(function (req, res) {
    res.sendfile(__dirname + '/client/index.html');
  });
});

app.listen(config.port, function () {
  console.log('API started on port', config.port)
});
