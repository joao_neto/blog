var config = {
  src_path: './client',
  components_path: '<%= config.src_path %>/components'
};

var mountFolder = function (connect, dir) {
  return connect.static(require('path').resolve(dir));
};

module.exports = function (grunt) {
  var bowerInstall = function () {
    var exec = require('child_process').exec,
        cb = this.async();
    grunt.log.writeln('Install bower components');
    exec('node_modules/.bin/bower install', { cwd: './' }, function (err, stdout, stderr) {
      grunt.log.writeln('Done.');
      cb();
    });
  };

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    config: config,

    watch: {
      server: {
        options: { livereload: true },
        files: ['<%= config.src_path %>/**/*']
      },
    },

    connect: {
      server: {
        options: {
          port: 9000,
          hostname: '0.0.0.0',
          livereload: true,
          middleware: function (connect) {
            return [mountFolder(connect, config.src_path)];
          }
        }
      },
    },

    clean: {
      install: { src: ['<%= config.components_path %>'] }
    },

    copy: {
      build: {
        files: [{ src: '**', dest: '<%= config.build_path %>' ,cwd: '<%= config.src_path %>', expand: true }]
      },
      test: {},
      release: {},
      cov: {},
      install: {
        files: [
          { expand: true,
            cwd: 'bower_components',
            src: [
              'jquery/jquery*.{js,map}',
              'ember*/*.js',
              'handlebars/*.js',
            ],
            dest: '<%= config.components_path %>' },
          { expand: true, cwd: 'bower_components/moment/min', src: ['**'], dest: '<%= config.components_path %>/moment' },
          { expand: true, cwd: 'bower_components/showdown/compressed', src: ['**'], dest: '<%= config.components_path %>/showdown' },
          { expand: true, cwd: 'bower_components/bootstrap/dist', src: ['**'], dest: '<%= config.components_path %>/bootstrap' },
        ]
      }
    },

  });

  grunt.registerTask('bower_install', 'install bower components', bowerInstall);
  grunt.registerTask('install',       'make install',  ['bower_install', 'clean:install', 'copy:install']);

  grunt.registerTask('start',         'start server',  ['install', 'connect', 'watch']);

};
