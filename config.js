var env    = process.env.NODE_ENV || 'development',
    secret = '$up3r-$ecr3t',
    salt   = 'common-$4lt';

switch (env) {
  case 'production':
    var db        = process.env.MONGOHQ_URL,
        port      = process.env.PORT || 3000;
    break;
  default:
  case 'development':
    var db        = 'mongodb://127.0.0.1/blog',
        port      = 3000;
    break;
  case 'test':
    var db        = 'mongodb://127.0.0.1/blog_test',
        port      = 3001;
    break;
};

module.exports.env = env;
module.exports.db = db;
module.exports.salt = salt;
module.exports.secret = secret;
module.exports.port = port;
