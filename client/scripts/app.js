var showdown = new Showdown.converter();

// workaround to use CORS withCredentials
// Ember.$.support.cors = true;
// Ember.$.ajaxPrefilter(function (options) {
//   options.xhrFields = { withCredentials: true };
// });

window.App = Ember.Application.create({
  currentUser: null,
  // apiUrl: 'http://localhost:3000/v1',
  apiUrl: '/v1',
  LOG_TRANSITIONS: true
});

// App.ApplicationAdapter = DS.RESTAdapter.extend({
//   host: 'http://localhost:3000',
//   namespace: 'v1'
// });


App.Ajax = Ember.Mixin.create({
  _settings: {},
  // _settings: { crossDomain: true, xhrFields: { withCredentials: true } },

  ajaxSuccessHandler: function (json) {
    // console.log(arguments);
    // App.set('successMessage', 'salvo com sucesso!');
    return json;
  },

  ajaxErrorHandler: function (json) {
    // console.log(arguments);
    App.set('errorMessage', json.responseJSON.error);
    return json;
  },

  cors: function (type, url, data, options) {
    var self = this;
    var settings = $.extend({}, this._settings, options || {});
    settings.url = App.get('apiUrl') + url;
    settings.type = type || 'GET';
    settings.data = data;

    return $.ajax(settings)
      .done(function () {
        return self.ajaxSuccessHandler.apply(self, $.makeArray(arguments));
      })
      .fail(function () {
        return self.ajaxErrorHandler.apply(self, $.makeArray(arguments));
      });
  }
});


App.Router.map(function () {
  this.resource('register');
  this.resource('account');
  this.resource('login');
  this.resource('logout');
  this.resource('posts', function () {
    this.resource('post', { path: ':post_id' });
    this.route('new');
  });
});

App.ApplicationRoute = Ember.Route.extend(App.Ajax, {
  setupController: function (controller, model) {
    console.log('Setup Controller');
    this._super(controller, model);
    controller.requestCurrentUser();
  },
  actions: {
    willTransition: function (transition) {
      var controller = this.controllerFor(transition.targetName);

      if (controller) {
        controller.set('successMessage', App.get('successMessage'));
        controller.set('errorMessage', App.get('errorMessage'));
        App.set('successMessage', null);
        App.set('errorMessage', null);
      }
    },
  //   error: function (reason, transition) {
  //     console.log(reason)
  //     if (reason.status === 401) {
  //       this.transitionToRoute('login');
  //     } else {
  //       console.log('error', transition)
  //     }
  //   }
  }
});

App.ApplicationController = Ember.Controller.extend(App.Ajax, {
  requestCurrentUser: function () {
    console.log('Request Current User');

    var loggedIn = localStorage['loggedIn'] && JSON.parse(localStorage['loggedIn']);

    if (loggedIn === true) {
      this.cors('GET', '/sessions/me', null, { cache: true }).then(function (res) {
        App.set('currentUser', res.session);
      });
    }
  },
  currentUser: function () {
    return App.get('currentUser')
  }.property('App.currentUser')
});

// App.NavbarController = App.ApplicationController.extend(App.Ajax, {
//   setupController: function (controller, model) {
//     console.log('Setup Controller');
//     this._super(controller, model);
//     controller.requestCurrentUser();
//   }
// })

Ember.Handlebars.helper('format-markdown', function (input) {
  if (input)
  return new Handlebars.SafeString(showdown.makeHtml(input));
});

Ember.Handlebars.helper('format-date', function (date) {
  if (date)
  return moment(date).fromNow();
});

