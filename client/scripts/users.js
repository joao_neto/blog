App.AccountController = App.ApplicationController.extend({
  password: '',
  errorMessage: '',

  actions: {
    reset: function () {
      this.setProperties({
        password: '',
        errorMessage: ''
      });
    },

    register: function () {
      var self = this;
      var data = {
        user: {
          password: this.get('password')
        }
      };

      this.cors('PUT', '/users/' + App.get('currentUser').userId, data)
        .then(function (res) {
          App.set('successMessage', 'Sua senha foi alterada com sucesso');
          self.transitionToRoute('index')
        }, function (res) {
          self.set('errorMessage', res.responseJSON.error);
        });
    }
  }
});

App.RegisterController = App.ApplicationController.extend({
  name: '',
  email: '',
  password: '',
  errorMessage: '',

  actions: {
    reset: function () {
      this.setProperties({
        name: '',
        email: '',
        password: '',
        errorMessage: null
      });
    },

    register: function () {
      var self = this;
      var data = {
        user: {
          name: this.get('name'),
          email: this.get('email'),
          password: this.get('password')
        }
      };

      this.cors('POST', '/users', data)
        .then(function (res) {
          App.set('successMessage', 'Usuário cadastrado com sucesso');
          self.transitionToRoute('index')
        }, function (res) {
          self.set('errorMessage', res.responseJSON.error);
        });
    }
  }
});
