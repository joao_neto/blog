App.LogoutRoute = App.ApplicationRoute.extend({
  setupController: function (controller) {
    var loggedIn = localStorage['loggedIn'] && JSON.parse(localStorage['loggedIn']);
    if (loggedIn === true) {
      this.cors('DELETE', '/sessions/me')
        .then(function (res) {
          localStorage['loggedIn'] = false;
          App.set('currentUser', null);
        });
    }

    controller.transitionToRoute('index');
  },
});

App.LoginRoute = App.ApplicationRoute.extend({});

App.LoginController = App.ApplicationController.extend({
  actions: {
    login: function () {
      var self = this;
      var data = {
        session: {
          email: this.get('email'),
          password: this.get('password')
        }
      };

      self.cors('POST', '/sessions', data)
        .then(function (res) {
          localStorage['loggedIn'] = true;
          App.set('currentUser', res.session);
          self.transitionToRoute('index');
        }, function (res) {
          App.set('currentUser', null);
          self.set('errorMessage', res.responseJSON.error);
        });
    }
  }
});

