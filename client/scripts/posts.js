App.PostsRoute = App.ApplicationRoute.extend({
  model: function () {
    return this.cors('GET', '/posts').then(function (res) {
      return res.posts.map(function (post) {
        return post;
      });
    });
  }
});

App.PostsController = App.ApplicationController.extend({
  needs: ['posts'],
  actions: {
    remove: function (post) {
      var self = this;
      this.cors('DELETE', '/posts/' + post._id).then(function () {
        App.set('successMessage', 'Post criado com sucesso');
        self.get('controllers.posts.content').removeObject(post);
        self.transitionToRoute('posts');
      });
    }
  }
});

App.PostRoute = App.ApplicationRoute.extend({
  model: function (params) {
    return this.cors('GET', '/posts/' + params.post_id).then(function (res) {
      var post = res.post;
      return post;
    });
  },
  afterModel: function (model) {
    var controller = this.controllerFor('post');
    controller.setProperties(model);
  }
});

App.PostController = App.ApplicationController.extend({
  isEditing: false,
  title: null,
  body: null,
  tags: null,

  actions: {
    edit: function () {
      this.set('isEditing', true);
    },

    done: function () {
      var self = this;
      var tags = this.get('tags').toString();
      var post_id = this.get('model')._id;
      var data = {
        post: {
          title: this.get('title'),
          body: this.get('body'),
          tags: tags ? tags.replace(/,\s+/g, ',').split(',') : []
        }
      };

      this.cors('PUT', '/posts/' + post_id, data).then(function () {
        App.set('successMessage', 'Post criado com sucesso');
        self.set('isEditing', false);
        self.transitionToRoute('post', post_id);
      });

    }
  }
});

App.PostsNewController = App.ApplicationController.extend({
  needs: ['posts'],
  title: null,
  body: null,
  tags: null,
  actions: {
    done: function () {
      var self = this;
      var tags = this.get('tags').toString();
      var data = {
        post: {
          title: this.get('title'),
          body: this.get('body'),
          comment: this.get('comment'),
          tags: tags ? tags.replace(/,\s+/g, '').split(',') : []
        }
      };

      this.cors('POST', '/posts', data).then(function (res) {
        App.set('successMessage', 'Post criado com sucesso');
        self.get('controllers.posts.content').pushObject(res.post);
        self.transitionToRoute('post', res.post._id);
      });
    }
  }
});
