var mongoose = require('mongoose');

module.exports = {
  load: function (fixture, callback) {
    var data = require('./' + fixture);
    var modelName = Object.keys(data)[0];
    return mongoose.model(modelName).create(data[modelName], callback);
  },
  clean: function (fixture) {

  }
};
