var should = require('should'),
    request = require('supertest'),
    mongoose = require('mongoose'),
    app = require('../app'),
    utils = require('./utils');

// after(utils.dropDatabase);

describe('Session', function () {
  var User;

  before(function (done) {
    User = require('../models/User');
    done();
  });

  describe('Functional test', function () {

    beforeEach(utils.beforeEachTest);

    it('should create session for valid user', function (done) {
      var email = 'user1@example.com';
      var password = '123';

      utils.login(email, password, function (err, cookies, res) {
        var session = res.body.session;
        session.should.have.property('userId');
        session.should.have.property('name');
        session.should.not.have.property('password');
        session.email.should.be.eql(email);
        done(err);
      });
    });

    it('should not create session for invalid user', function (done) {
      utils.login('noop', 'noop', function (err, cookies, res) {
          var session = res.body;
          session.should.have.property('error');
          done(err);
        });
    });

    it('should delete session for logout user request', function (done) {
      var email = 'user1@example.com';
      var password = '123';

      utils.login(email, password, function (err, cookies, res) {
        request(app)
          .del('/v1/sessions/me')
          .set('Cookie', cookies)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            var session = res.body;
            session.should.have.property('message');
            session.should.not.have.property('error');
            done(err);
          });
      });
    });

    it('should return message when delete dummy session', function (done) {
      request(app)
        .del('/v1/sessions/me')
        .set('Accept','application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          var session = res.body;
          session.should.have.property('message');
          session.should.not.have.property('error');
          done(err);
        });
    });

    it('should get session', function (done) {
      var email = 'user1@example.com';
      var password = '123';

      utils.login(email, password, function (err, cookies, res) {
        request(app)
          .get('/v1/sessions/me')
          .set('Cookie', cookies)
          .set('Accept','application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            var session = res.body.session;
            session.should.have.property('userId');
            session.should.have.property('name');
            session.should.not.have.property('password');
            session.email.should.be.eql(email);
            done(err);
          });
      });
    });

    it('should not get session for unauthorized user', function (done) {
      request(app)
        .get('/v1/sessions/me')
        .set('Accept','application/json')
        .expect('Content-Type', /json/)
        .expect(401)
        .end(function (err, res) {
          var session = res.body;
          session.should.have.property('error');
          done(err);
        });
    });

  });
});
