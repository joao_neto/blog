var mongoose = require('mongoose'),
    fixtures = require('../fixtures'),
    request = require('supertest'),
    app = require('../../app'),
    User = mongoose.model('User'),
    Post = mongoose.model('Post');

module.exports = {
  beforeEachTest: function (done) {
    User.remove({}, function () {
      Post.remove({}, function () {
        fixtures.load('users', function () {
          fixtures.load('posts', done);
        });
      })
    });
  },
  dropDatabase: function (done) {
    mongoose.connection.db.dropDatabase(function (err) {
      done(err);
    });
  },
  login: function (email, password, done) {
    request(app)
      .post('/v1/sessions')
      .set('Accept','application/json')
      .send({ session: { email: email, password: password } })
      .end(function (err, res) {
        var cookies = res.headers['set-cookie'].pop().split(';')[0];
        return done(err, cookies, res);
      });
  }
}
