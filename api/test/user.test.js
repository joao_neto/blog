var should = require('should'),
    request = require('supertest'),
    mongoose = require('mongoose'),
    app = require('../app'),
    utils = require('./utils');

after(utils.dropDatabase);

describe('User', function () {
  var User;

  before(function (done) {
    User = require('../models/User');
    done();
  });

  // Unit tests
  describe('Unit test', function () {
    beforeEach(utils.beforeEachTest);

    it('should compare password', function (done) {
      var hashedPassword = User.hashPassword('123');
      var user = new User({ password: hashedPassword });
      var result = user.comparePassword('123');
      result.should.be.true;
      done();
    });

    it('should return false comparing invalid password', function (done) {
      var hashedPassword = User.hashPassword('INVALID');
      var user = new User({ password: hashedPassword });
      var result = user.comparePassword('123');
      result.should.be.false;
      done();
    });

    it('should authenticate user with valid email and password', function (done) {
      var email = 'user1@example.com';
      var password = '123';
      var hashedPassword = User.hashPassword(password);

      User.authenticate(email, password, function (err, user) {
        user.email.should.be.eql(email);
        user.password.should.be.eql(hashedPassword);
        done(err);
      });
    });
  });

  // Functional tests
  describe('Functional test', function () {
    beforeEach(utils.beforeEachTest);

    it('should create user', function (done) {
      var name = 'Foo Bar'
      var email = 'foo@example.com';
      var password = 'abc';

      request(app)
        .post('/v1/users')
        .set('Accept', 'application/json')
        .send({ user: { name: name, email: email, password: password } })
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          var user = res.body.user;
          user.should.have.property('_id');
          user.should.have.property('name');
          user.should.not.have.property('password');
          user.name.should.be.eql(name);
          user.email.should.be.eql(email);
          done(err);
        });
    });

    it('should update user password', function (done) {
      var email = 'user1@example.com';
      var password = '123';
      var newPassword = 'abc1234';
      var hashedNewPassword = User.hashPassword(newPassword);

      utils.login(email, password, function (err, cookies, res) {
        var session = res.body.session;
        var userId = session.userId;
        var userName = session.name;

        request(app)
          .put('/v1/users/' + userId)
          .set('Cookie', cookies)
          .set('Accept', 'application/json')
          .send({ user: { name: 'blabla', email: 'lol@example.com', password: newPassword } })
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            User.findById(userId, function (err, user) {
              user.name.should.be.eql(userName);
              user.email.should.be.eql(email);
              user.password.should.not.eql(password);
              user.password.should.be.eql(hashedNewPassword);
              done(err);
            });
          });
      });
    });

    it('should view user', function (done) {
      var email = 'user1@example.com';
      var password = '123';

      utils.login(email, password, function (err, cookies, res) {
        var session = res.body.session;
        var userId = session.userId;

        request(app)
          .get('/v1/users/' + userId)
          .set('Cookie', cookies)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            var user = res.body.user;
            user.should.have.property('_id');
            user.should.have.property('name');
            user.email.should.be.eql(email);
            user.should.not.have.property('password');
            done(err);
          });
      });
    });

    it('should not view user for unauthorized user', function (done) {
      request(app)
        .get('/v1/users/52acf059183b6ef626000111')
        .set('Accept','application/json')
        .expect('Content-Type', /json/)
        .expect(401)
        .end(function (err, res) {
          var result = res.body;
          result.should.have.property('error');
          done(err);
        });
    });

  });
});
