var should = require('should'),
    request = require('supertest'),
    mongoose = require('mongoose'),
    app = require('../app'),
    utils = require('./utils');

// mongoose.set('debug', true)

// after(utils.dropDatabase);

describe('Post', function () {
  var Post;

  before(function (done) {
    User = require('../models/User');
    Post = require('../models/Post');
    done();
  });

  // Unit tests
  /*
  describe('Unit test', function () {
    beforeEach(utils.beforeEachTest);
  });
  */

  // Functional tests
  describe('Functional test', function () {
    beforeEach(utils.beforeEachTest);

    it('should create post', function (done) {
      var title = 'My Post';
      var body = 'Hello world, this is my post!';
      var tags = ['Hello World', 'My Post'];

      utils.login('user1@example.com', '123', function (err, cookies, res) {
        var session = res.body.session;
        var userId = session.userId;

        request(app)
          .post('/v1/posts')
          .set('Cookie', cookies)
          .set('Accept', 'application/json')
          .send({ post: { title: title, body: body, tags: tags } })
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            var post = res.body.post;
            post.should.have.property('_id');
            post.should.have.property('created');
            post.title.should.be.eql(title);
            post.body.should.be.eql(body);
            post.tags.should.be.eql(tags);
            done(err);
          });
      });
    });

    it('should update post', function (done) {
      var title = 'My Updated Post';
      var body = 'Hello world, this is my updated post!';
      var tags = ['Hello World', 'My Post', 'More Tags'];

      utils.login('user2@example.com', '321', function (err, cookies, res) {
        var session = res.body.session;

        request(app)
          .put('/v1/posts/abcde059183b6ef626000222')
          .set('Cookie', cookies)
          .set('Accept', 'application/json')
          .send({ post: { title: title, body: body, tags: tags } })
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            var post = res.body.post;
            post.should.have.property('_id');
            post.should.have.property('user');
            post.should.have.property('created');
            post.should.have.property('updated');
            post.title.should.be.eql(title);
            post.body.should.be.eql(body);
            post.tags.should.be.eql(tags);
            done(err);
          });
      });
    });

    it('should not update post for not owner user', function (done) {
      var title = 'My Updated Post';
      var body = 'Hello world, this is my updated post!';
      var tags = ['Hello World', 'My Post', 'More Tags'];

      utils.login('user1@example.com', '123', function (err, cookies, res) {
        request(app)
          .put('/v1/posts/abcde059183b6ef626000222')
          .set('Cookie', cookies)
          .set('Accept', 'application/json')
          .send({ post: { title: title, body: body, tags: tags } })
          .expect('Content-Type', /json/)
          .expect(401)
          .end(function (err, res) {
            var result = res.body;
            result.should.have.property('error');
            done(err);
          });
      });
    });

    it('should view post', function (done) {
      request(app)
        .get('/v1/posts/abcde059183b6ef626000222')
        .set('Accept','application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          var post = res.body.post;
          post.should.have.property('_id');
          post.title.should.be.eql('Post 2');
          post.should.have.property('body');
          post.should.have.property('tags');
          post.should.have.property('created');
          post.should.not.have.property('updated');
          done(err);
        });
    });

    it('should view post inexistent return error', function (done) {
      request(app)
        .get('/v1/posts/dummy0000000000000000000')
        .set('Accept','application/json')
        .expect('Content-Type', /json/)
        .expect(404)
        .end(function (err, res) {
          var result = res.body;
          result.should.have.property('error');
          done(err);
        });
    });

    it('should list post order by updated and created', function (done) {
      request(app)
        .get('/v1/posts')
        .query({ title: 'Post', tags: ['nodejs'] })
        .set('Accept','application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          var posts = res.body.posts;
          posts[0].title.should.be.eql('Post 2');
          posts[1].title.should.be.eql('Post 1');
          done(err);
        });
    });

    it('should list post filtered by "foo" tag', function (done) {
      request(app)
        .get('/v1/posts')
        .query({ tags: ['foo'] })
        .set('Accept','application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          var posts = res.body.posts;
          posts[0].title.should.be.eql('Post 2');
          should.not.exists(posts[1]);
          done(err);
        });
    });

    it('should delete post', function (done) {
      utils.login('user2@example.com', '321', function (err, cookies, res) {
        request(app)
          .del('/v1/posts/abcde059183b6ef626000222')
          .set('Cookie', cookies)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            var result = res.body;
            result.should.have.property('message');
            done(err);
          });
      });
    });

    it('should create post comment', function (done) {
      utils.login('user2@example.com', '321', function (err, cookies, res) {
        var session = res.body.session;
        var userId = session.userId;
        var commentBody = 'my comment';

        request(app)
          .post('/v1/posts/abcde059183b6ef626000222/comments')
          .set('Cookie', cookies)
          .send({ comment: { body: commentBody } })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            var comment = res.body.comment;
            comment.body.should.be.eql(commentBody);
            comment.user.should.be.eql(userId);
            done(err);
          });
      });
    });

    it('should update post comment', function (done) {
      utils.login('user1@example.com', '123', function (err, cookies, res) {
        var session = res.body.session;
        var commentBody = 'Awesome!';

        request(app)
          .put('/v1/posts/abcde059183b6ef626000111/comments/ccccc059183b6ef626222111')
          .set('Cookie', cookies)
          .send({ comment: { body: commentBody } })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            var comment = res.body.comment;
            comment.body.should.be.eql(commentBody);
            comment.should.have.property('created');
            comment.should.have.property('updated');
            done(err);
          });
      });
    });

    it('should update inexistent post comment return error', function (done) {
      utils.login('user1@example.com', '123', function (err, cookies, res) {
        var session = res.body.session;

        request(app)
          .put('/v1/posts/abcde059183b6ef626000111/comments/dummy0000000000000000000')
          .set('Cookie', cookies)
          .send({ comment: { body: 'dummy' } })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(404)
          .end(function (err, res) {
            var result = res.body;
            result.should.have.property('error');
            done(err);
          });
      });
    });

    it('should view post comment', function (done) {
      var commentId = 'ccccc059183b6ef626222111';

      request(app)
        .get('/v1/posts/abcde059183b6ef626000111/comments/' + commentId)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          var comment = res.body.comment;
          comment._id.should.be.eql(commentId);
          comment.should.have.property('body');
          comment.should.have.property('created');
          done(err);
        });
    });

    it('should delete post comment', function (done) {
      utils.login('user2@example.com', '321', function (err, cookies, res) {
        var session = res.body.session;
        var commentId = 'ccccc059183b6ef626222111';

        request(app)
          .del('/v1/posts/abcde059183b6ef626000111/comments/' + commentId)
          .set('Cookie', cookies)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            Post.findById('abcde059183b6ef626000111', function (err, post) {
              post.comments[0]._id.should.be.not.eql(commentId);
              should.not.exists(post.comments[1]);
              done(err);
            });
          });
      });
    });

    it('should list post comment', function (done) {
      utils.login('user2@example.com', '321', function (err, cookies, res) {
        var session = res.body.session;

        request(app)
          .get('/v1/posts/abcde059183b6ef626000111/comments')
          .set('Cookie', cookies)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            var comments = res.body.comments;
            comments.length.should.be.eql(2);
            done(err);
          });
      });
    });

  });
});
