var express     = require('express'),
    mongoose    = require('mongoose'),
    mongoStore  = require('connect-mongo')(express),
    config      = require('../config'),
    app         = module.exports = express();

require('./exceptions');

app.configure(function () {
  app.use(express.urlencoded());
  app.use(express.json());
  app.use(express.cookieParser());

  if (config.env !== 'test') {
    app.use(express.logger('dev'));
  }

  app.use(express.methodOverride());
  app.use(express.favicon());
  app.use(express.session({
    secret: config.secret,
    store: new mongoStore({ url: config.db })
  }));

  app.use(app.router);

  app.use(function clientErrorHandler(err, req, res, next) {
    // console.log(err.message)
    res.json(err.status || 500, { error: err.message });
  });
});

// Load models
require('./models');

// Load API resources
require('./resources')(app);

// app.listen(config.port, function () {
//   console.log('API started on port', config.port)
// });
