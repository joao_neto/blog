var mongoose = require('mongoose');
var User = mongoose.model('User');

module.exports = {
  view: function (req, res, next) {
    return res.json({ 'session': req.session._user });
  },
  create: function (req, res, next) {
    var session = req.body && req.body.session ? req.body.session : null;

    if (req.session._user) {
      return res.json({ 'session': req.session._user });
    }

    if (!session || !session.email || !session.password) {
      return next(new BadRequest);
    }

    User.authenticate(session.email, session.password, function (err, user) {
      if (err) {
        return next(err);
      }

      req.session._user = {
        userId: user._id,
        name: user.name,
        email: user.email
      };

      return res.json({ 'session': req.session._user });
    });
  },
  destroy: function (req, res, next) {
    if (!req.session) {
      return res.json({ message: 'You are not logged in' });
    }

    req.session.destroy(function (err) {
      if (err) {
        return next(new InternalServiceError);
      }

      return res.json({ message: 'Good bye!' });
    });
  }
};
