/*
    it('should create post', function (done) {
      Post.aggregate([{ $unwind: '$tags' }, { $group: { _id: '$tags' } }], function () {
        console.log(arguments);
        done()
      });
    });
*/
var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var User = mongoose.model('User');

module.exports = {
  isOwner: function (req, res, next) {
    var userId = req.session._user.userId;
    var post = req.post;

    if (userId !== post.user.toString()) {
      return next(new Unauthorized('You are not owner'));
    }

    return next();
  },
  load: function (req, res, next) {
    var postId = req.params.postId;

    if (!postId) {
      return next(new BadRequest);
    }

    Post.findOne({ _id: postId }, { comments: false }, function (err, post) {
      if (err) {
        return next(new InternalServiceError);
      }

      if (!post) {
        return next(new NotFound('Post not found'));
      }

      req.post = post;
      return next();
    });
  },
  index: function (req, res, next) {
    var query = Post.find({}).sort({ updated: -1, created: -1 });

    if (req.query.title) {
      query.where('title').regex(new RegExp('^' + req.query.title, 'i'));
    }

    if (req.query.tags) {
      query.where('tags').in(req.query.tags);
    }

    query.exec(function (err, posts) {
      return res.json({ 'posts': posts });
    });
  },
  view: function (req, res, next) {
    return res.json({ 'post': req.post });
  },
  create: function (req, res, next) {
    var post = new Post(req.body.post);

    post.user = req.session._user.userId;
    post.save(function (err, post) {
      if (err) {
        return next(new InternalServiceError);
      }

      return res.json({ 'post': post });
    });
  },
  update: function (req, res, next) {
    var post = req.post;

    post.title = req.body.post.title;
    post.body = req.body.post.body;
    post.tags = req.body.post.tags;
    post.updated = new Date();
    post.save(function (err, post) {
      if (err) {
        return next(new InternalServiceError);
      }

      return res.json({ 'post': post });
    });
  },
  destroy: function (req, res, next) {
    var post = req.post;

    post.remove(function (err) {
      if (err) {
        return next(new InternalServiceError);
      }

      return res.json({ message: 'Post deleted' });
    });
  }
};
