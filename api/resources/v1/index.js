module.exports = function (app) {
  var sessionsApi = require('./sessions'),
      usersApi = require('./users'),
      postsApi = require('./posts'),
      postsCommentsApi = require('./postsComments');

  // Sessions API
  app.get('/v1/sessions/me',
    usersApi.ensureLogin,
    sessionsApi.view
  );
  app.post('/v1/sessions',
    sessionsApi.create
  );
  app.delete('/v1/sessions/me',
    sessionsApi.destroy
  );

  // Users API
  app.get('/v1/users',
    usersApi.index
  );
  app.get('/v1/users/:userId',
    usersApi.ensureLogin,
    usersApi.load,
    usersApi.view
  );
  app.post('/v1/users',
    usersApi.create
  );
  app.put('/v1/users/:userId',
    usersApi.ensureLogin,
    usersApi.load,
    usersApi.update
  );
  app.delete('/v1/users',
    usersApi.destroy
  );

  // Posts API
  app.get('/v1/posts',
    postsApi.index
  );
  app.get('/v1/posts/:postId',
    postsApi.load,
    postsApi.view
  );
  app.post('/v1/posts',
    usersApi.ensureLogin,
    postsApi.create
  );
  app.put('/v1/posts/:postId',
    usersApi.ensureLogin,
    postsApi.load,
    postsApi.isOwner,
    postsApi.update
  );
  app.delete('/v1/posts/:postId',
    usersApi.ensureLogin,
    postsApi.load,
    postsApi.isOwner,
    postsApi.destroy
  );

  // Posts/:postId/comments API
  app.get('/v1/posts/:postId/comments',
    postsCommentsApi.index
  );
  app.get('/v1/posts/:postId/comments/:commentId',
    postsApi.load,
    postsCommentsApi.load,
    postsCommentsApi.view
  );
  app.post('/v1/posts/:postId/comments',
    postsApi.load,
    usersApi.ensureLogin,
    postsCommentsApi.create
  );
  app.put('/v1/posts/:postId/comments/:commentId',
    usersApi.ensureLogin,
    postsApi.load,
    postsCommentsApi.load,
    postsCommentsApi.isOwner,
    postsCommentsApi.update
  );
  app.delete('/v1/posts/:postId/comments/:commentId',
    usersApi.ensureLogin,
    postsApi.load,
    postsCommentsApi.load,
    postsCommentsApi.isOwner,
    postsCommentsApi.destroy
  );

};
