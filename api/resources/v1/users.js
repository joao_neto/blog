var mongoose = require('mongoose');
var User = mongoose.model('User');

module.exports = {
  ensureLogin: function (req, res, next) {
    if (!req.session || !req.session._user) {
      return next(new Unauthorized);
    }

    return next();
  },
  load: function (req, res, next) {
    var userId = req.params.userId;

    if (!userId) {
      return next(new BadRequest);
    }

    User.findById(userId, function (err, user) {
      if (err) {
        return next(new InternalServiceError);
      }

      req.user = user;
      return next();
    });
  },
  index: function (req, res, next) {
    return next(InternalServiceError('not implemented'));
  },
  view: function (req, res, next) {
    if (!req.user) {
      return next(new NotFound);
    }

    return res.json({ 'user': req.user });
  },
  create: function (req, res, next) {
    var user = new User(req.body.user);
    user.save(function (err, user) {
      if (err) {
        return next(new InternalServiceError);
      }

      return res.json({ 'user': user });
    });
  },
  update: function (req, res, next) {
    var user = req.user;
    var newPassword = req.body.user.password;

    user.password = newPassword;
    user.save(function (err, user) {
      if (err) {
        return next(new InternalServiceError);
      }

      return res.json({ 'user': user });
    });
  },
  destroy: function (req, res, next) {
    return next(InternalServiceError('not implemented'));
  }
};
