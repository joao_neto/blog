var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var User = mongoose.model('User');
var PostComment = mongoose.model('PostComment');

module.exports = {
  isOwner: function (req, res, next) {
    var userId = req.session._user.userId;

    if (userId !== req.post.user.toString() && userId !== req.comment.user.toString()) {
      return next(new Unauthorized('You are not owner'));
    }

    return next();
  },
  load: function (req, res, next) {
    var postId = req.params.postId;
    var commentId = req.params.commentId;

    if (!postId || !commentId) {
      return next(new BadRequest);
    }

    Post.findOne({ _id: postId }, { comments: true }, function (err, post) {
      if (err) {
        return next(new InternalServiceError);
      }

      req.comment = post.comments.id(commentId);
      if (!req.comment) {
        return next(new NotFound('Comment not found'));
      }

      return next();
    });
  },
  index: function (req, res, next) {
    var postId = req.params.postId;

    if (!postId) {
      return next(new BadRequest);
    }

    Post.findById(postId, function (err, post) {
      return res.json({ 'comments': post.comments });
    });
  },
  view: function (req, res, next) {
    // deep populate
    User.findById(req.post.user, function (err, user) {
      var _comment = req.comment;
      _comment.user = user;
      return res.json({ 'comment': _comment });
    });
  },
  create: function (req, res, next) {
    var post = req.post;
    var comment = new PostComment({
      user: req.session._user.userId,
      body: req.body.comment.body
    });

    post.comments.push(comment);
    post.save(function (err, post) {
      if (err) {
        return next(new InternalServiceError);
      }

      /*
      // Deep comment user populate
      User.findById(comment.user).exec(function (err, user) {
        var _comment = comment.toJSON();
        _comment.user = user;
        return res.json({ 'comment': _comment });
      });
      */

      return res.json({ 'comment': comment });
    });
  },
  update: function (req, res, next) {
    var post = req.post;
    var comment = req.comment;

    comment.body = req.body.comment.body;
    comment.updated = new Date();

    // post.updated = new Date();
    post.comments.push(comment);
    post.save(function (err, post) {
      if (err) {
        return next(new InternalServiceError);
      }

      return res.json({ 'comment': comment });
    });
  },
  destroy: function (req, res, next) {
    var postId = req.post._id;
    var commentId = req.comment._id;

    Post.findById(postId, { comments: true }, function (err, post) {
      if (err) {
        return next(new InternalServiceError);
      }

      post.comments.id(commentId).remove();
      post.save(function (err, post) {
        if (err) {
          return next(new InternalServiceError);
        }

        return res.json({ message: 'Comment deleted' });
      });
    });
  }
};
