var mongoose = require('mongoose');

var PostCommentSchema = require('./PostComment');

var PostSchema = new mongoose.Schema({
  title: { type: String, trim: true, index: true },
  body: String,
  user: { type: mongoose.Schema.ObjectId, ref: 'User', required: true },
  tags: { type: [String], index: true },
  comments: [PostCommentSchema],
  created: { type: Date, 'default': Date.now },
  updated: Date
});

// Properties
PostSchema.statics.PostCommentSchema = PostCommentSchema;

// Methods

// Hooks
PostSchema.pre('save', function (next) {
  next();
});

module.exports = mongoose.model('Post', PostSchema);
