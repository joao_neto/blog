/**
 * Auto-load bundled models
 */

var fs = require('fs'),
    config = require('../../config'),
    mongoose = require('mongoose');

fs.readdirSync(__dirname).forEach(function (file) {
  if (file !== 'index.js')
    require(__dirname + '/' + file);
});

// Connect to mongo with mongoose
mongoose.connect(config.db);
