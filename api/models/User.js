var mongoose = require('mongoose'),
    crypto   = require('crypto'),
    config   = require('../../config');

var UserSchema = new mongoose.Schema({
  name: { type: String, required: true, trim: true },
  email: { type: String, lowercase: true, index: { unique: true, sparse: true }, required: true },
  password: { type: String }
});

// Methods
UserSchema.statics.hashPassword = function (password) {
  return crypto.createHash('md5').update(password).update(config.salt).digest('hex');
};

UserSchema.methods.comparePassword = function (password) {
  var hashPassword = this.model('User').hashPassword(password);
  return this.password === hashPassword;
};

UserSchema.statics.authenticate = function (email, password, next) {
  this.findOne({ email: email }, function (err, user) {
    if (err) {
      return next(new InternalServiceError);
    }

    // !user handle user not found
    if (!user || !user.comparePassword(password)) {
      return next(new BadRequest('Invalid email or password'));
    }

    return next(null, user);
  });
};

// Hooks
UserSchema.pre('save', function (next) {
  this.password = this.model('User').hashPassword(this.password);
  next();
});

UserSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    var retJson = {
      _id: ret._id,
      name: ret.name,
      email: ret.email
    };
    return retJson;
  }
});

module.exports = mongoose.model('User', UserSchema);
