var mongoose = require('mongoose');

var PostCommentSchema = module.exports = new mongoose.Schema({
  user: { type: mongoose.Schema.ObjectId, ref: 'User', required: true },
  body: String,
  created: { type: Date, 'default': Date.now },
  updated: Date
});

// Methods

// Hooks
PostCommentSchema.pre('save', function (next) {
  next();
});

mongoose.model('PostComment', PostCommentSchema);
